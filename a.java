package zengqiangest;

import java.io.IOException;

import com.sun.tools.attach.AgentInitializationException;
import com.sun.tools.attach.AgentLoadException;
import com.sun.tools.attach.AttachNotSupportedException;
import com.sun.tools.attach.VirtualMachine;

public class Attacher {
    public static void main(String[] args) throws AttachNotSupportedException, IOException, AgentLoadException, AgentInitializationException {
        // 传入目标 JVM pid
    	String path= args[0];
    	String pid = args[1];
    	
        VirtualMachine vm = VirtualMachine.attach(pid); 
        vm.loadAgent(path);
    }
}